require 'test_helper'

class CashierControllerTest < ActionDispatch::IntegrationTest
  test "should get add" do
    get cashier_add_url
    assert_response :success
  end

  test "should get remove" do
    get cashier_remove_url
    assert_response :success
  end

  test "should get update_quantity" do
    get cashier_update_quantity_url
    assert_response :success
  end

  test "should get close_client" do
    get cashier_close_client_url
    assert_response :success
  end

  test "should get index" do
    get cashier_index_url
    assert_response :success
  end

end
