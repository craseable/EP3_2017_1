class CashierController < ApplicationController

  def index
     @cashier = session[:cashier] || {}
  end

  def add
    name = params[:name]
    id = params[:id]
    cashier = session[:cashier] ||= {}
    cashier[id] = (cashier[id] || 0) + 1

    redirect_to :action => :index
  end

  def remove
  end

  def update_quantity
  end

  def close_client
  end


end
