Rails.application.routes.draw do
  get 'cashier/add'

  get 'cashier/remove'

  get 'cashier/update_quantity'

  get 'cashier/close_client'

  get 'cashier/index'

  resources :items
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
